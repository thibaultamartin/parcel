use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use laposte::TrackingNumber;
use libhandy::prelude::*;
use std::convert::TryFrom;

use crate::application::Action;
use crate::config::{APP_ID, PROFILE};
use crate::widgets::ParcelsListWidget;
use crate::window_state;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum View {
    ParcelsList,
    NewParcel,
    Syncing(bool),
}

pub struct Window {
    pub widget: libhandy::ApplicationWindow,
    builder: gtk::Builder,
    sender: Sender<Action>,
    settings: gio::Settings,
    actions: gio::SimpleActionGroup,
    pub parcels_list: ParcelsListWidget,
}

impl Window {
    pub fn new(sender: Sender<Action>) -> Self {
        let settings = gio::Settings::new(APP_ID);

        let builder = gtk::Builder::new_from_resource("/in/thibaultmart/Parcel/window.ui");
        get_widget!(builder, libhandy::ApplicationWindow, window);

        let actions = gio::SimpleActionGroup::new();

        let window_widget = Window {
            widget: window,
            builder,
            settings,
            actions,
            parcels_list: ParcelsListWidget::new(sender.clone()),
            sender,
        };

        window_widget.init();
        window_widget.init_views();
        window_widget.setup_actions();
        window_widget
    }

    fn init(&self) {
        // Devel Profile
        if PROFILE == "Devel" {
            self.widget.get_style_context().add_class("devel");
        }

        // load latest window state
        window_state::load(&self.widget, &self.settings);

        // save window state on delete event
        self.widget.connect_delete_event(clone!(@strong self.settings as settings => move |window, _| {
            if let Err(err) = window_state::save(&window, &settings) {
                warn!("Failed to save window state, {}", err);
            }
            Inhibit(false)
        }));
    }

    fn init_views(&self) {
        // Home page
        self.parcels_list.bind_model();
        get_widget!(self.builder, gtk::Box, home_page_main_box);
        home_page_main_box.pack_end(&self.parcels_list.widget, true, true, 0);

        // Add entry page
        get_widget!(self.builder, gtk::Entry, parcel_number_entry);
        get_widget!(self.builder, gtk::Entry, parcel_name_entry);
        parcel_number_entry.connect_changed(clone!(@strong self.builder as builder => move |entry| {
            if let Some(text) = entry.get_text() {
                get_widget!(builder, gtk::Button, save_new_parcel_button);
                match TrackingNumber::try_from(text.as_str().trim()) {
                    Ok(_) => {
                        entry.get_style_context().remove_class("error");
                        parcel_name_entry.set_sensitive(true);
                        save_new_parcel_button.set_sensitive(true);
                    },
                    Err(_) => {
                        entry.get_style_context().add_class("error");
                        parcel_name_entry.set_sensitive(false);
                        save_new_parcel_button.set_sensitive(false);
                    },
                }
            }
        }));
    }

    pub fn set_view(&self, view: View) {
        get_widget!(self.builder, libhandy::Deck, pages_deck);
        match view {
            View::ParcelsList => {
                pages_deck.set_visible_child_name("parcels-list");
            }
            View::NewParcel => {
                get_widget!(self.builder, gtk::Entry, parcel_number_entry);
                parcel_number_entry.grab_focus_without_selecting();
                get_widget!(self.builder, gtk::Button, save_new_parcel_button);
                save_new_parcel_button.grab_default();
                pages_deck.set_visible_child_name("new-parcel");
            }
            View::Syncing(state) => {
                get_widget!(self.builder, gtk::ProgressBar, loading_progress);
                loading_progress.set_visible(state);
                if !state {
                    // If we hide the progress bar
                    loading_progress.set_fraction(0.0); // Reset the fraction
                } else {
                    loading_progress.pulse();
                    gtk::timeout_add(200, move || {
                        loading_progress.pulse();
                        glib::Continue(loading_progress.get_visible())
                    });
                }
            }
        }
    }

    fn setup_actions(&self) {
        action!(
            self.widget,
            "save-parcel",
            clone!(@strong self.sender as sender, @strong self.builder as builder => move |_, _| {
                debug!("Adding parcel number");

                get_widget!(builder, gtk::Entry, parcel_number_entry);
                get_widget!(builder, gtk::Entry, parcel_name_entry);
                let tracking_number = parcel_number_entry.get_text().unwrap();

                // Note: this will crash if the tracking number is not valid. This is on purpose,
                // since invalid numbers should never make it here. If they do this must be fixed.
                let tracking_number = TrackingNumber::try_from(tracking_number.trim()).unwrap();
                let name = parcel_name_entry.get_text().unwrap().trim().to_string();

                if name.is_empty() {
                    send!(sender, Action::AddParcel(tracking_number));
                } else {
                    send!(sender, Action::AddNamedParcel(tracking_number, name));
                }

                send!(sender, Action::PreviousView);
                parcel_number_entry.set_text("");
            })
        );
    }

    pub fn previous_view(&self) {
        self.set_view(View::ParcelsList);
    }
}
