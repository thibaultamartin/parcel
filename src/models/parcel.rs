use chrono::DateTime;
use chrono::{naive::NaiveDateTime, offset::Utc};
use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
use diesel_derive_enum::DbEnum;
use failure::Error;
use glib::Cast;
use serde::{Deserialize, Serialize};
use std::convert::{From, TryFrom};

use crate::database;
use crate::models::{Carrier, ObjectWrapper};
use crate::schema::parcels;

#[derive(Clone, Debug, Deserialize, Serialize, DbEnum)]
pub enum ParcelStatus {
    ReadyToBeCollectedByCarrier,
    CollectedByCarrier,
    InTransit,
    PickedUpForCrossingBorder,
    CrossingBorder,
    HandedToCustoms,
    ReleasedByCustoms,
    HeldByCustoms,
    ProblemOccurring,
    ReadyForDelivery,
    CannotDistribute,
    WaitingInPostOffice,
    ReturnedToSender,
    Delivered,
    DeliveredToSender,
}

impl From<laposte::status::DeliveryStatus> for ParcelStatus {
    fn from(status: laposte::status::DeliveryStatus) -> Self {
        match status {
            laposte::status::DeliveryStatus::ReadyToBeCollectedByCarrier => ParcelStatus::ReadyToBeCollectedByCarrier,
            laposte::status::DeliveryStatus::CollectedByCarrier => ParcelStatus::CollectedByCarrier,
            laposte::status::DeliveryStatus::InTransit => ParcelStatus::InTransit,
            laposte::status::DeliveryStatus::ReadyForDelivery => ParcelStatus::ReadyForDelivery,
            laposte::status::DeliveryStatus::Delivered => ParcelStatus::Delivered,
        }
    }
}

impl From<laposte::status::EventStatus> for ParcelStatus {
    fn from(status: laposte::status::EventStatus) -> Self {
        use laposte::status::EventStatus;

        match status {
            EventStatus::Declared => ParcelStatus::ReadyToBeCollectedByCarrier,
            EventStatus::CollectedByCarrier => ParcelStatus::CollectedByCarrier,
            EventStatus::CollectedInShippingCountry => ParcelStatus::CollectedByCarrier,
            EventStatus::UnderTreatment => ParcelStatus::CollectedByCarrier,
            EventStatus::UnderTreatmentInShippingCountry => ParcelStatus::CollectedByCarrier,
            EventStatus::UnderTreatmentInDestinationCountry => ParcelStatus::InTransit,
            EventStatus::UnderTreatmentInTransitCountry => ParcelStatus::InTransit,
            EventStatus::WaitingForPresentation => ParcelStatus::InTransit,
            EventStatus::HandedToCustoms => ParcelStatus::HandedToCustoms,
            EventStatus::ReleasedByCustoms => ParcelStatus::ReleasedByCustoms,
            EventStatus::HeldByCustoms => ParcelStatus::HeldByCustoms,
            EventStatus::ProblemOccurring => ParcelStatus::ProblemOccurring,
            EventStatus::ProblemSolved => ParcelStatus::InTransit,
            EventStatus::SetForDistribution => ParcelStatus::ReadyForDelivery,
            EventStatus::CannotDistribute => ParcelStatus::CannotDistribute,
            EventStatus::WaitingInPostOffice => ParcelStatus::WaitingInPostOffice,
            EventStatus::ReturnedToSender => ParcelStatus::ReturnedToSender,
            EventStatus::Delivered => ParcelStatus::Delivered,
            EventStatus::DeliveredToSender => ParcelStatus::DeliveredToSender,
        }
    }
}

impl From<ParcelStatus> for std::string::String {
    fn from(status: ParcelStatus) -> std::string::String {
        match status {
            // TODO use translatable strings
            ParcelStatus::ReadyToBeCollectedByCarrier => String::from("Ready to be collected by carrier"),
            ParcelStatus::CollectedByCarrier => String::from("Collected by carrier"),
            ParcelStatus::InTransit => String::from("In transit"),
            ParcelStatus::PickedUpForCrossingBorder => String::from("Picked up for crossing border"),
            ParcelStatus::CrossingBorder => String::from("Crossing border"),
            ParcelStatus::HandedToCustoms => String::from("Handed to customs"),
            ParcelStatus::ReleasedByCustoms => String::from("Released by Customs"),
            ParcelStatus::HeldByCustoms => String::from("Held by customs"),
            ParcelStatus::ProblemOccurring => String::from("Problem occurring"),
            ParcelStatus::ReadyForDelivery => String::from("Ready for delivery"),
            ParcelStatus::CannotDistribute => String::from("Cannot distribute the parcel"),
            ParcelStatus::WaitingInPostOffice => String::from("Waiting in post office"),
            ParcelStatus::ReturnedToSender => String::from("Parcel is being returned to sender"),
            ParcelStatus::Delivered => String::from("Delivered"),
            ParcelStatus::DeliveredToSender => String::from("Delivered back to sender"),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, Insertable, Queryable, AsChangeset)]
#[table_name = "parcels"]
pub struct ParcelEntity {
    pub tracking_number: String,
    pub carrier: Carrier,
    pub name: Option<String>,
    pub status: ParcelStatus,
    pub created_at_utc: NaiveDateTime,
    pub shipped_at_utc: Option<NaiveDateTime>,
    pub last_updated_utc: Option<NaiveDateTime>,
}

impl TryFrom<laposte::TrackingInfo> for ParcelEntity {
    type Error = &'static str;

    fn try_from(tracking_info: laposte::TrackingInfo) -> Result<Self, Self::Error> {
        let tracking_number = laposte::TrackingNumber::try_from(tracking_info.shipment.id_ship.as_str()).unwrap().to_string();

        let status = match tracking_info.shipment.last_event() {
            Some(event) => ParcelStatus::try_from(event.code).unwrap(),
            None => ParcelStatus::try_from(tracking_info.shipment.timeline).unwrap(),
        };

        let name = None;
        let now = Utc::now().naive_utc();
        let created_at_utc = now; // TODO should be equal to either now or the time retrieved from DB
        let shipped_at_utc = match tracking_info.shipment.shipping_event() {
            Some(event) => Some(DateTime::parse_from_rfc3339(&event.date).unwrap().naive_utc()),
            None => None,
        };
        let last_updated_utc = Some(now); // TODO should be retrieved from api response

        let carrier = match tracking_info.shipment.holder {
            1 => Carrier::LaPoste,
            2 => Carrier::LaPoste,
            3 => Carrier::Chronopost,
            4 => Carrier::Colissimo,
            _ => Carrier::LaPoste,
        };

        Ok(ParcelEntity {
            tracking_number,
            carrier,
            name,
            status,
            created_at_utc,
            shipped_at_utc,
            last_updated_utc,
        })
    }
}

impl ParcelEntity {
    pub fn compare(a: &glib::Object, b: &glib::Object) -> std::cmp::Ordering {
        let parcel_a: ParcelEntity = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
        let parcel_b: ParcelEntity = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

        parcel_b.created_at_utc.cmp(&parcel_a.created_at_utc)
    }

    pub fn update_or_insert(&self) -> Result<(), Error> {
        use crate::schema::parcels::dsl::*;
        let db = database::connection();
        let conn = db.get()?;

        let results = parcels
            .filter(tracking_number.eq(&self.tracking_number))
            .filter(carrier.eq(&self.carrier))
            .load::<ParcelEntity>(&conn)?;
        if !results.is_empty() {
            let target = parcels.filter(tracking_number.eq(&self.tracking_number)).filter(carrier.eq(&self.carrier));
            diesel::update(target).set(self).execute(&conn)?;
        } else {
            diesel::insert_into(parcels).values(self).execute(&conn)?;
        }

        Ok(())
    }

    pub fn load() -> Result<Vec<Self>, Error> {
        use crate::schema::parcels::dsl::*;
        let db = database::connection();
        let conn = db.get()?;

        let results = parcels.load::<ParcelEntity>(&conn)?;
        debug!("Results: {}", results.len());
        Ok(results)
    }
}
