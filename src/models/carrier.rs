use diesel_derive_enum::DbEnum;
use serde::{Deserialize, Serialize};
use std::convert::From;

#[derive(Clone, Debug, Serialize, Deserialize, DbEnum)]
pub enum Carrier {
    LaPoste,
    Chronopost,
    Colissimo,
}

impl From<Carrier> for std::string::String {
    fn from(carrier: Carrier) -> std::string::String {
        match carrier {
            Carrier::LaPoste => String::from("La Poste"),
            Carrier::Chronopost => String::from("Chronopost"),
            Carrier::Colissimo => String::from("Colissimo"),
        }
    }
}
