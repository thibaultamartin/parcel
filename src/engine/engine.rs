use crate::application::Action;
use crate::models::{Carrier, ParcelEntity};
use futures::future::join_all;
use glib::Sender;
use laposte::{TrackingInfo, TrackingNumber};
use std::convert::TryFrom;
use std::vec::Vec;

#[derive(Clone)]
pub struct Engine {
    sender: Sender<Action>,
    laposteclient: laposte::Client,
}

impl Engine {
    pub fn new(sender: Sender<Action>) -> Self {
        // TODO actually retrieve credentials from the environment variable
        let okapi_key = "J5LrG/4Q9yXNjU/xLQX7eJFZaW9tDc7f/iC9CDNWRdwc2GgKTZVBXjlEupA3NDtM";
        let laposteclient = laposte::Client::new(okapi_key);

        Engine { sender, laposteclient }
    }

    pub async fn sync(&self) {
        debug!("Launching a synchronization");
        let parcels_from_db = ParcelEntity::load().unwrap();

        for parcel in parcels_from_db {
            // TODO handle what should happen if the tracking number became invalid (e.g. if the
            // carrier removed it from its system after a while)
            let tracking_number = TrackingNumber::try_from(parcel.tracking_number.as_str()).unwrap();
            let tracking_info = self.laposteclient.get_tracking_info(tracking_number).await;

            match tracking_info {
                // unwrap() is safe here, since the lib did all the checks. If the API didn't
                // answer as expected, we need the app to crash
                Ok(info) => {
                    let mut parcel_updated = ParcelEntity::try_from(info).unwrap();
                    if let Some(name) = parcel.name {
                        parcel_updated.name = Some(name);
                    }
                    send!(self.sender, Action::UpdateOrInsert(parcel_updated));
                }
                // TODO handle the API errors
                Err(e) => debug!("Error with futures: {}", e),
            }
        }
        debug!("Synchronization is over");
    }

    pub async fn add_named_parcel(&self, tracking_number: laposte::TrackingNumber, name: String) {
        debug!("Lauching synchronization for a single parcel");
        let tracking_info = self.laposteclient.get_tracking_info(tracking_number);

        match tracking_info.await {
            Ok(info) => {
                let mut parcel = ParcelEntity::try_from(info).unwrap();
                if name.is_empty() {
                    parcel.name = None;
                } else {
                    parcel.name = Some(name);
                }
                send!(self.sender, Action::UpdateOrInsert(parcel))
            }
            // TODO handle API errors
            Err(e) => debug!("Error with API: {}", e),
        }
    }
}
