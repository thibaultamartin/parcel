table! {
    use diesel::sql_types::*;
    use crate::models::CarrierMapping;
    use crate::models::ParcelStatusMapping;
    parcels (tracking_number, carrier) {
        tracking_number -> Text,
        carrier -> CarrierMapping,
        name -> Nullable<Text>,
        status -> ParcelStatusMapping,
        created_at_utc -> Timestamp,
        shipped_at_utc -> Nullable<Timestamp>,
        last_updated_utc -> Nullable<Timestamp>,
    }
}
