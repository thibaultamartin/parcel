#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate gtk_macros;

use gettextrs::*;

#[macro_use]
mod utils;

mod application;
mod config;
mod static_resources;
mod window;
mod window_state;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

use libhandy::Column;
mod database;
mod engine;
mod models;
mod schema;
mod widgets;

fn main() {
    pretty_env_logger::init();

    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    glib::set_application_name(&format!("Parcel{}", config::NAME_SUFFIX));
    glib::set_prgname(Some("parcel"));

    gtk::init().expect("Unable to start GTK3");
    Column::new(); // Workaround since we can't call hdy_init();

    static_resources::init().expect("Failed to initialize the resource file.");

    let app = Application::new();
    app.run(app.clone());
}
