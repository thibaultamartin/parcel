use gio::prelude::*;
use glib::{Receiver, Sender};
use gtk::prelude::*;
use laposte::TrackingNumber;
use std::env;
use std::{cell::RefCell, rc::Rc};

use crate::config;
use crate::engine::Engine;
use crate::models::ParcelEntity;
use crate::window::View;
use crate::window::Window;

pub enum Action {
    SetView(View),
    PreviousView,
    Sync,
    AddParcel(TrackingNumber),
    AddNamedParcel(TrackingNumber, String),
    UpdateOrInsert(ParcelEntity),
}

pub struct Application {
    app: gtk::Application,
    engine: Engine,
    sender: Sender<Action>,
    receiver: RefCell<Option<Receiver<Action>>>,
    window: Window,
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::FLAGS_NONE).unwrap();
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RefCell::new(Some(r));

        let window = Window::new(sender.clone());
        let engine = Engine::new(sender.clone());

        let application = Rc::new(Self {
            app,
            engine,
            window,
            sender,
            receiver,
        });

        application.init();
        send!(application.sender, Action::Sync);
        application
    }

    fn init(&self) {
        self.setup_widgets();
        self.setup_gactions();
        self.setup_signals();
        self.setup_css();
        send!(self.sender, Action::SetView(View::ParcelsList));
    }

    fn setup_widgets(&self) {
        let builder = gtk::Builder::new_from_resource("/in/thibaultmart/Parcel/shortcuts.ui");
        get_widget!(builder, gtk::ShortcutsWindow, shortcuts);
        self.window.widget.set_help_overlay(Some(&shortcuts));
    }

    fn setup_gactions(&self) {
        // Quit
        action!(
            self.app,
            "quit",
            clone!(@strong self.app as app => move |_, _| {
                app.quit();
            })
        );
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);

        // About
        action!(
            self.app,
            "about",
            clone!(@weak self.window.widget as window => move |_, _| {
                let builder = gtk::Builder::new_from_resource("/in/thibaultmart/Parcel/about_dialog.ui");
                get_widget!(builder, gtk::AboutDialog, about_dialog);
                about_dialog.set_transient_for(Some(&window));

                about_dialog.connect_response(|dialog, _| dialog.destroy());
                about_dialog.show();

            })
        );

        action!(
            self.app,
            "add-parcel",
            clone!(@strong self.sender as sender => move |_, _| {
                send!(sender, Action::SetView(View::NewParcel));
            })
        );

        action!(
            self.app,
            "previous",
            clone!(@strong self.sender as sender => move |_, _| {
                send!(sender, Action::PreviousView);
            })
        );

        action!(
            self.app,
            "sync",
            clone!(@strong self.sender as sender => move |_, _| {
                send!(sender, Action::Sync);
            })
        );

        // Keyboard shortcuts
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>comma"]);
        self.app.set_accels_for_action("app.sync", &["F5"]);
        self.app.set_accels_for_action("app.previous", &["Escape"]);
    }

    fn setup_signals(&self) {
        self.app.connect_activate(clone!(@weak self.window.widget as window => move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        }));
    }

    fn setup_css(&self) {
        if let Some(theme) = gtk::IconTheme::get_default() {
            theme.add_resource_path("/in/thibaultmart/Parcel/icons");
        }

        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/in/thibaultmart/Parcel/style.css");
        if let Some(screen) = gdk::Screen::get_default() {
            gtk::StyleContext::add_provider_for_screen(&screen, &p, 500);
        }
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("Parcel:{} ({})", config::NAME_SUFFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            /* UI */
            Action::SetView(view) => self.window.set_view(view),
            Action::PreviousView => self.window.previous_view(),
            Action::Sync => self.sync(),
            Action::UpdateOrInsert(parcel) => self.window.parcels_list.update_or_insert(parcel),
            Action::AddParcel(tracking_number) => self.add_parcel(tracking_number),
            Action::AddNamedParcel(tracking_number, name) => self.add_named_parcel(tracking_number, name),
        };
        glib::Continue(true)
    }

    fn sync(&self) {
        send!(self.sender, Action::SetView(View::Syncing(true)));

        let engine = self.engine.clone();
        let sender = self.sender.clone();
        spawn!(async move {
            info!("Starting a new sync");
            engine.sync().await;
            send!(sender, Action::SetView(View::Syncing(false)));
        });
    }

    fn add_parcel(&self, tracking_number: TrackingNumber) {
        self.add_named_parcel(tracking_number, String::new());
    }

    fn add_named_parcel(&self, tracking_number: TrackingNumber, name: String) {
        send!(self.sender, Action::SetView(View::Syncing(true)));

        let engine = self.engine.clone();
        let sender = self.sender.clone();
        spawn!(async move {
            info!("Starting a new sync for a single named tracking number");
            engine.add_named_parcel(tracking_number, name).await;
            send!(sender, Action::SetView(View::Syncing(false)));
        });
    }
}
