use crate::application::Action;
use crate::models::{ObjectWrapper, ParcelEntity};
use crate::widgets::ParcelRow;

use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use laposte::TrackingNumber;
use std::io::{Error, ErrorKind};

pub struct ParcelsListWidget {
    builder: gtk::Builder,
    sender: Sender<Action>,
    pub widget: gtk::Stack,
    model: gio::ListStore,
}

impl ParcelsListWidget {
    pub fn new(sender: Sender<Action>) -> Self {
        let builder = gtk::Builder::new_from_resource("/in/thibaultmart/Parcel/list.ui");
        get_widget!(builder, gtk::Stack, parcels_list_stack);
        let model = gio::ListStore::new(ObjectWrapper::static_type());

        ParcelsListWidget {
            builder,
            sender,
            widget: parcels_list_stack,
            model,
        }
    }

    pub fn bind_model(&self) {
        get_widget!(self.builder, gtk::Stack, parcels_list_stack);
        if self.model.get_n_items() == 0 {
            parcels_list_stack.set_visible_child_name("empty-list");
        } else {
            parcels_list_stack.set_visible_child_name("parcels-list");
        }

        self.model.connect_items_changed(move |model, _, _, _| {
            if model.get_n_items() == 0 {
                parcels_list_stack.set_visible_child_name("empty-list");
            } else {
                parcels_list_stack.set_visible_child_name("parcels-list");
            }
        });

        get_widget!(self.builder, gtk::ListBox, parcels_list);
        parcels_list.bind_model(Some(&self.model), move |parcel| {
            let parcel: ParcelEntity = parcel.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            let row = ParcelRow::new(parcel);
            row.widget.upcast::<gtk::Widget>()
        });
    }

    pub fn remove(&self, parcel: ParcelEntity) -> Result<(), Error> {
        let position = self.position(&parcel.tracking_number)?;
        self.model.remove(position);
        Ok(())
    }

    pub fn update_or_insert(&self, parcel: ParcelEntity) {
        let position = self.position(&parcel.tracking_number);
        let mut insert_at = 0;
        if let Ok(position) = position {
            self.model.remove(position);
            insert_at = position;
        };

        let object = ObjectWrapper::new(Box::new(parcel.clone()));
        self.model.insert(insert_at, &object);
        parcel.update_or_insert(); // Updates or inserts the parcel in database
    }

    fn position(&self, tracking_number: &str) -> Result<u32, Error> {
        for i in 0..self.model.get_n_items() {
            let parcel: ParcelEntity = self.model.get_object(i).unwrap().downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            if parcel.tracking_number == tracking_number {
                return Ok(i);
            }
        }

        Err(Error::new(ErrorKind::NotFound, "Tracking number not (yet?) referenced"))
    }
}
