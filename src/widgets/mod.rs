pub mod parcels_list;
pub mod parcels_list_row;

pub use parcels_list::ParcelsListWidget;
pub use parcels_list_row::ParcelRow;
