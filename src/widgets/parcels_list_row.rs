use crate::models::{Carrier, ParcelEntity};
use chrono::format::strftime::StrftimeItems;
use gtk::prelude::*;

pub struct ParcelRow {
    pub widget: gtk::ListBoxRow,
    builder: gtk::Builder,
    parcel: ParcelEntity,
}

impl ParcelRow {
    pub fn new(parcel: ParcelEntity) -> Self {
        let builder = gtk::Builder::new_from_resource("/in/thibaultmart/Parcel/parcel_card.ui");
        get_widget!(builder, gtk::ListBoxRow, parcel_list_row);

        let row = Self {
            widget: parcel_list_row,
            builder,
            parcel,
        };

        row.init();
        row
    }

    fn init(&self) {
        get_widget!(self.builder, gtk::Label, parcel_card_name);
        match &self.parcel.name {
            Some(name) => parcel_card_name.set_text(&name),
            None => parcel_card_name.set_text(&self.parcel.tracking_number),
        }

        get_widget!(self.builder, gtk::Label, parcel_card_status);
        parcel_card_status.set_text(&String::from(&String::from(self.parcel.status.clone())));

        get_widget!(self.builder, gtk::Label, parcel_card_shipped_at);
        match &self.parcel.shipped_at_utc {
            Some(shipped_at_utc) => {
                let format_str = "%A %B %d";
                let date_str = shipped_at_utc.format(format_str);
                let label_text = format!("Shipped on {}", date_str);
                parcel_card_shipped_at.set_text(&label_text);
            }
            None => parcel_card_shipped_at.set_text("Shipping date not provided"),
        }

        get_widget!(self.builder, gtk::Image, parcel_card_carrier_logo);
        match self.parcel.carrier {
            Carrier::LaPoste => parcel_card_carrier_logo.set_from_icon_name(Some("la-poste"), gtk::IconSize::Dialog),
            Carrier::Chronopost => parcel_card_carrier_logo.set_from_icon_name(Some("chronopost"), gtk::IconSize::Dialog),
            Carrier::Colissimo => parcel_card_carrier_logo.set_from_icon_name(Some("colissimo"), gtk::IconSize::Dialog),
        }
    }
}
