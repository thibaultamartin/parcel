# Check-list

* [ ] The carrier offers a REST API (SOAP APIs cannot be handled properly with Rust as of today)
* [ ] The API is free of charge
* [ ] The API is documented in English
* [ ] I understand this is a project maintained for fun and nobody owes me anything

# Links

* [Developper Platform Registration](https://insert.your.link/here)
* [API Documentation](https://insert.your.link/here)
