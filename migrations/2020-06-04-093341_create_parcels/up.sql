CREATE TABLE parcels (
	tracking_number VARCHAR NOT NULL,
	carrier TEXT NOT NULL,
	name VARCHAR,
	status TEXT NOT NULL,
	created_at_utc DATETIME,
	shipped_at_utc DATETIME,
	last_updated_utc DATETIME,
	PRIMARY KEY(tracking_number, carrier)
)
