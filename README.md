# An app to track deliveries

Parcel will allow you to track your packages delivery. The roadmap can be [tracked here](https://gitlab.gnome.org/thibaultamartin/parcel/-/milestones)

<div align="center">
![Main Window](data/resources/screenshots/screenshot1.png "Main Window")
![Add Number](data/resources/screenshots/screenshot2.png "Add Number")
![Track Number](data/resources/screenshots/screenshot3.png "Track Number")
</div>

# Can you support new carriers?

This project is maintained for fun, not for profit. Adding carriers takes time, and making sure nothing breaks in the future is both important and time consuming.

You can open an issue and use the `add_carrier` template to provide all the information needed to support a carrier. If I have the time and motivation to do so, I may add support for the carrier at some point. The best way to get a carrier support added is to maintain a library yourself so I can effortlessly embed it in Parcels.

# How to contribute?

To build the development version of Read It Later and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You can also join [Parcel's matrix room](https://matrix.to/#/!DxtpbRRUMgfxjRNhke:gnome.org?via=gnome.org&via=t2bot.io&via=matrix.org)

You are expected to follow our [Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct) when participating in project
spaces.

I don't accept tips as I already have a day job. If you want to contribute financially, you can give to:
* [The gtk-rs team](https://opencollective.com/gtk-rs) for all their work that allows us to use GTK with Rust
* [Bilal Elmoussaoui](https://www.paypal.me/BilalELMoussaoui) for his tremendous amount of work on libhandy-rs, which allows us to use mobile-friendly widgets, his application template which saves a lot of time, and all the time he put in helping me getting started on Parcel
* [Alexander Mikhaylenko](https://www.paypal.me/exalm) for the insane amount of work he put in libhandy, which allows us to use mobile-friendly widgets
* [The GNOME Foundation](https://www.gnome.org/support-gnome/donate/) who supports all the infrastructure costs and organizes GNOME related events

# Credits

Thanks a lot to Bilal Elmoussaoui for his [GTK Rust Template](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template),
Adrien Plazas and  Alexander Mikhailenko for their work on libhandy, and Tobias
Bernard for his awesome design work. 
